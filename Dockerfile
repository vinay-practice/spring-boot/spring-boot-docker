#FROM openjdk:8
#ADD build/libs/demo-0.0.1.jar spring-boot-docker.jar
#EXPOSE 8080
#ENTRYPOINT ["java","-jar","spring-boot-docker.jar"]


#FROM openjdk:8
#VOLUME /tmp
#COPY build/libs/demo-0.0.1.jar demo-0.0.1.jar
#ENTRYPOINT ["java","-jar","/demo-0.0.1.jar"]

#FROM java:8-jdk-alpine
#ADD build/libs/demo-0.0.1.jar //
#ENTRYPOINT ["java","-jar","/demo-0.0.1.jar"]

FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY build/libs/demo-0.0.1.jar demo-0.0.1.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/demo-0.0.1.jar"]