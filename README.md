# spring-boot-docker

Program which contain basic steps to create docker file for spring boot application.

# How to create docker image

Execute below command:
docker build -f <dockerfile-path> -t <iamge-name> .

e.g.
docker build -f G:\workspace\spring-boot\spring-boot-docker\src\main\resources\Dockerfile -t spring-boot-docker .

# How to get docker-machine ip

docker-machine ip

Above command provide docker machine ip.

# How to run docker image

docker run -p port-number-for-ext-world:port-number-inside-container docker-image-name

e.g.
docker run -p 8080:8080 docker-image-name

# How to pull docker image from docker hub

1. docker login
2. docker pull vinay01tech/vinay01tech:docker-first


# How to acces spring API

Open browser just browse
docker-machine-ip:8080/message

e.g.
192.168.99.100:8080/message

# How to execute inside 