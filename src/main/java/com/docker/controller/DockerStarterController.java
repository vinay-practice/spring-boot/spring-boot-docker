package com.docker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DockerStarterController {

    @GetMapping("/message")
    public String printDockerMessage()
    {
        return "Welcome to docker starter";
    }

}
